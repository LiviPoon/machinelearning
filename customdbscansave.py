"""
Custom Implementation of DBSCAN
By Livi Poon

Course Notes:

DBSCAN works like this

> For each point that does not yet have a label
    > count how many neighbors it has (points that are within the maximum distance from it)
    > if there are fewer than the mininum number of neighbors, label it as noise and move on to the next point
    > otherwise, give this core point a new cluster label
    > for each of this point's neighbor points
        > if the neighbor already has a label other than noise, move on to the next neighbor
        > otherwise, give the neighbor the same new cluster label
        > if the neighbor has more than the minimum number of neighbors, add all of them to the list of neighbors for this cluster, so that they go through this same loop

"""

import numpy
import math as m
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
from mpl_toolkits.mplot3d import Axes3D
import random
from sklearn import datasets
from sklearn import preprocessing
from sklearn import metrics
import pandas
import sys

iris = datasets.load_iris() #load dataframe
iris_df = pandas.DataFrame(iris.data) #create a pandas dataframe
iris_df.columns = iris.feature_names #add a column that defines names

#holds all the data points
coordsX = []
coordsY = []
coords = []
#---

history = [] #holds all the datapoints that have been run through regression already
cluster = [] #holds all the points of the cluster
throwaway = [] #used to determine if the circle has the minPoints

for i in range(len(iris.data)): #extracting data from the pandas dataframe
    coordsX.append(iris.data[i][0]) #assigning different values to their respected lists
    coordsY.append(iris.data[i][1])
    coords.append([iris.data[i][0],iris.data[i][1]]) #create an array of x and y coordinates

def dbscan(epsilon, minPoints): #main dbscan function
    global coords #globalize various variables used globally
    global history
    global cluster
    global throwaway

    history = [] #wipe reused/throwaway lists
    cluster = []
    throwaway = []

    #BELOW: error check
    if epsilon <= 0:
        print("ERROR - Epsilon value is less than or equal to 0: " + str(epsilon))
        return

    if str(type(minPoints)) == "int":
        print("ERROR - minPoint value is not a natural number: " + str(minPoints))
        return
    #---

    selectedPoint = random.randrange(len(coords)) #get a random point from the data set to start dbscan

    #BELOW: initialize dbscan by gathering the points around the selectedPoint
        #usage: to prevent running regression on points that do not fit the minPoints criteria
    for i in range(len(coords)):
        #BELOW: to double check if the points are within the epsilon circle 
        #solve sqrt((x2 - x1)**2+(y2 + y2)**2) = c, where c is the distance from point a to point b.
        x = (coords[i][0] - coords[selectedPoint][0]) ** 2
        y = (coords[i][1] - coords[selectedPoint][1]) ** 2
        if m.sqrt(x + y) <= epsilon:
            cluster.append(coords[i])
            throwaway.append(coords[i]) #append to throwaway list
        #---
    #---

    #BELOW: regression loop 1
    if len(throwaway) >= minPoints: #begin regression by checking if the amount of
        for i in range(len(cluster)): #for everypoint in cluster
            if cluster[i] in history: #check if the regression has been run on this particular point already
                pass #pass this point if so
            else:
                dbscanRecurrsion(epsilon, minPoints, coords.index(cluster[i])) #if not, run dbscanRecurrsion
    #---

    coords = [x for x in coords if x not in cluster] #remove values that have been assigned to a cluster already

    clusterX = [] #create lists to hold x and y values of the clusters for plotting
    clusterY = []

    for i in range(len(cluster)):
        clusterX.append(cluster[i][0]) #extract x and y values
        clusterY.append(cluster[i][1])

    colors = list(mcolors.CSS4_COLORS) #random colors
    plt.scatter(clusterX,clusterY, c=random.choice(colors)) #plot

    #BELOW: regression loop 2
    if len(coords) == 0: #if the coords list is empty (no more datapoints to assign to a cluster) then the loop stops
        pass
    else:
        dbscan(epsilon, minPoints) #countinue to the next cluster.
    #---


def dbscanRecurrsion(epsilon, minPoints, selectedPoint):
    global coords #globalize variables
    global cluster
    global throwaway
    global history

    history.append(coords[selectedPoint]) #append the point
    throwaway = [] #clear throwaway list

    for i in range(len(coords)): #repeat epsilon check as seen in the dbscan function
        x = (coords[i][0] - coords[selectedPoint][0]) ** 2
        y = (coords[i][1] - coords[selectedPoint][1]) ** 2
        if m.sqrt(x + y) <= epsilon:
            cluster.append(coords[i])
            throwaway.append(coords[i])

dbscan(0.5, 3) #run dbscan
plt.show() #show the plot

